+++
title = "Learning Path"
menuTitle = "Learning Path"
weight = 1

+++
---

<!-- wrapper for learning paths -->

{{<flexwrap>}}

<!-- row for 101 bar -->


<!-- bar container for 101 level -->
{{<flexbar201>}}

Learning Objectives - 201



<!-- bar container for 201 level -->
{{<flexbar101>}}

Learning Objectives - 101






<!-- row for concept level -->


<!-- concept containers  -->


{{<flexbarconcept>}}
Ways of Working

<!-- Class container for ways of working -->
{{<flexbarclass>}}
Engineering Process

<!-- engineering process items -->
{{<flexbaritem>}}
Discovery
{{</flexbaritem>}}

{{<flexbaritem>}}
Psychological Safety
{{</flexbaritem>}}

{{<flexbaritem>}}
Pair Programming
{{</flexbaritem>}}

{{<flexbaritem>}}
Mobbing
{{</flexbaritem>}}

{{<flexbaritem>}}
Empowerment


{{</flexbaritem>}}
<!-- end engineering process items -->



{{</flexbarclass>}}


{{<flexbarclass>}}
Source Control

<!-- Source Control process items -->
{{<flexbaritem>}}
GitLab
{{</flexbaritem>}}

{{<flexbaritem>}}
Branching
{{</flexbaritem>}}

{{<flexbaritem>}}
Code Reviews
{{</flexbaritem>}}

{{<flexbaritem>}}
Conventional Commits
{{</flexbaritem>}}

<!-- Source Control process items -->

{{</flexbarclass>}}


{{<flexbarclass>}}
Agile

<!-- Agile process items -->
{{<flexbaritem>}}
GitLab Issues
{{</flexbaritem>}}

{{<flexbaritem>}}
Rotating SM
{{</flexbaritem>}}

{{<flexbaritem>}}
Ceremonies
{{</flexbaritem>}}

{{<flexbaritem>}}
Mini Charter
{{</flexbaritem>}}

{{<flexbaritem>}}
Focus
{{</flexbaritem>}}

{{<flexbaritem>}}
Commitment
{{</flexbaritem>}}

<!-- Product items -->
{{</flexbarclass>}}
{{<flexbarclass>}}
Product

<!-- Product items -->
{{<flexbaritem>}}
User Empathy 
{{</flexbaritem>}}

{{<flexbaritem>}}
Value Focus
{{</flexbaritem>}}

{{<flexbaritem>}}
Personas
{{</flexbaritem>}}

{{<flexbaritem>}}
Minimum Viable Product
{{</flexbaritem>}}

{{<flexbaritem>}}
Experimentation
{{</flexbaritem>}}


<!-- Agile process items -->
{{</flexbarclass>}}

<!-- end ways of working row -->


{{</flexbarconcept>}}
<!-- end engineering process items -->



{{<flexbarconcept>}}
Operations / Infra

<!-- Class container for ways of working -->
{{<flexbarclass>}}
Engineering Process

<!-- engineering process items -->
{{<flexbaritem>}}
Discovery
{{</flexbaritem>}}

{{<flexbaritem>}}
Psychological Safety
{{</flexbaritem>}}

{{<flexbaritem>}}
Pair Programming
{{</flexbaritem>}}

{{<flexbaritem>}}
Mobbing
{{</flexbaritem>}}

{{<flexbaritem>}}
Empowerment
{{</flexbaritem>}}
<!-- end engineering process items -->

{{</flexbarclass>}}


{{<flexbarclass>}}
Source Control

<!-- Source Control process items -->
{{<flexbaritem>}}
GitLab
{{</flexbaritem>}}

{{<flexbaritem>}}
Branching
{{</flexbaritem>}}

{{<flexbaritem>}}
Code Reviews
{{</flexbaritem>}}

{{<flexbaritem>}}
Conventional Commits
{{</flexbaritem>}}

<!-- Source Control process items -->

{{</flexbarclass>}}


{{<flexbarclass>}}
Agile

<!-- Agile process items -->
{{<flexbaritem>}}
GitLab Issues
{{</flexbaritem>}}

{{<flexbaritem>}}
Rotating SM
{{</flexbaritem>}}

{{<flexbaritem>}}
Ceremonies
{{</flexbaritem>}}

{{<flexbaritem>}}
Mini Charter
{{</flexbaritem>}}

{{<flexbaritem>}}
Focus
{{</flexbaritem>}}

{{<flexbaritem>}}
Commitment
{{</flexbaritem>}}

<!-- Product items -->
{{</flexbarclass>}}
{{<flexbarclass>}}
Product

<!-- Product items -->
{{<flexbaritem>}}
User Empathy 
{{</flexbaritem>}}

{{<flexbaritem>}}
Value Focus
{{</flexbaritem>}}

{{<flexbaritem>}}
Personas
{{</flexbaritem>}}

{{<flexbaritem>}}
Minimum Viable Product
{{</flexbaritem>}}

{{<flexbaritem>}}
Experimentation
{{</flexbaritem>}}


<!-- Agile process items -->
{{</flexbarclass>}}



<!-- end ways of working row -->

{{</flexbarconcept>}}

{{</flexbar101>}}

{{<flexbarconcept>}}
App Dev


<!-- Class container for ways of working -->
{{<flexbarclass>}}
Engineering Process

<!-- engineering process items -->
{{<flexbaritem>}}
Discovery
{{</flexbaritem>}}

{{<flexbaritem>}}
Psychological Safety
{{</flexbaritem>}}

{{<flexbaritem>}}
Pair Programming
{{</flexbaritem>}}

{{<flexbaritem>}}
Mobbing
{{</flexbaritem>}}

{{<flexbaritem>}}
Empowerment
{{</flexbaritem>}}
<!-- end engineering process items -->

{{</flexbarclass>}}


{{<flexbarclass>}}
Source Control

<!-- Source Control process items -->
{{<flexbaritem>}}
GitLab
{{</flexbaritem>}}

{{<flexbaritem>}}
Branching
{{</flexbaritem>}}

{{<flexbaritem>}}
Code Reviews
{{</flexbaritem>}}

{{<flexbaritem>}}
Conventional Commits
{{</flexbaritem>}}

<!-- Source Control process items -->

{{</flexbarclass>}}



{{<flexbarclass>}}
Agile

<!-- Agile process items -->
{{<flexbaritem>}}
GitLab Issues
{{</flexbaritem>}}

{{<flexbaritem>}}
Rotating SM
{{</flexbaritem>}}

{{<flexbaritem>}}
Ceremonies
{{</flexbaritem>}}

{{<flexbaritem>}}
Mini Charter
{{</flexbaritem>}}

{{<flexbaritem>}}
Focus
{{</flexbaritem>}}

{{<flexbaritem>}}
Commitment
{{</flexbaritem>}}

<!-- Product items -->
{{</flexbarclass>}}
{{<flexbarclass>}}
Product

<!-- Product items -->
{{<flexbaritem>}}
User Empathy 
{{</flexbaritem>}}

{{<flexbaritem>}}
Value Focus
{{</flexbaritem>}}

{{<flexbaritem>}}
Personas
{{</flexbaritem>}}

{{<flexbaritem>}}
Minimum Viable Product
{{</flexbaritem>}}

{{<flexbaritem>}}
Experimentation
{{</flexbaritem>}}



<!-- Agile process items -->
{{</flexbarclass>}}




<!-- end ways of working row -->
{{</flexbarconcept>}}

{{</flexbar201>}}

{{</flexwrap>}}